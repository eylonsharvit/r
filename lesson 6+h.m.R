setwd("C:/Users/�����/OneDrive/������/bikes")
bikes.data.raw <-read.csv("bike sharing.csv")

str(bikes.data.raw)
library(ggplot2)

bikes.data.prepared <- bikes.data.raw

# turn data time into charcter
bikes.data.prepared$datetime <- as.character(bikes.data.prepared$datetime)
str(bikes.data.prepared)


Sys.setlocale("LC_TIME","English")
date <- substr(bikes.data.prepared$datetime, 1, 10)
days <- weekdays(as.Date(date))

#add weekdays
bikes.data.prepared$weekday <- as.factor(days)
str(bikes.data.prepared)

#add month
month <- as.integer(substr(date, 6, 7))
bikes.data.prepared$month <- month

#add year
year <- as.integer(substr(date, 1, 4))
bikes.data.prepared$year <- year

hour <- as.integer(substr(bikes.data.prepared$datetime, 12, 13))
bikes.data.prepared$hour <- hour

#find seasons
table(bikes.data.prepared$season, bikes.data.prepared$month)

#turn season into factor
bikes.data.prepared$season <- factor(bikes.data.prepared$season, levels = 1:4, labels = c('winter', 'spring', 'summer', 'automn' ))

#Q1
bikes.data.prepared$weather <- factor(bikes.data.prepared$weather, levels = 1:4, labels = c('clear', 'mist', 'light snow', 'havy rain' ))
str(bikes.data.prepared$weather)

#Q4
library(ggplot2)
# Basic box plot
p <- ggplot(bikes.data.prepared, aes(x=season, y=count)) + 
  geom_boxplot()
p

#Q5
p <- ggplot(bikes.data.prepared, aes(x=weather, y=count)) + 
  geom_boxplot()
p
���� ����� �� ����� �� ������� ������� ������ ��� ���. ����� ������� ����� ��� ���� ���� ����� ��� �����

#look at time impact
ggplot(bikes.data.prepared, aes(as.factor(year), count)) + geom_boxplot()
ggplot(bikes.data.prepared, aes(as.factor(month), count)) + geom_boxplot()
ggplot(bikes.data.prepared, aes(day, count)) + geom_boxplot()
ggplot(bikes.data.prepared, aes(as.factor(hour), count)) + geom_boxplot()

#bining hour
breaks <- c(0,7,8,9,17,20,21,22,24) 
labels <- c("0-6","7","8","9-16","17-19","20","21","22-23")
bins <- cut(bikes.data.prepared$hour, breaks, include.lowest = T, right = F, labels = labels)

#Add to data frame
bikes.data.prepared$hourb <- bins
str(bikes.data.prepared)
ggplot(bikes.data.prepared, aes(as.factor(hourb), count)) + geom_boxplot()

#how temperature affects count
ggplot(bikes.data.prepared, aes(temp, count)) + geom_point() + stat_smooth(method = lm)

#how wind affects count
ggplot(bikes.data.prepared, aes(windspeed, count)) + geom_point() + stat_smooth(method = lm)
str(bikes.data.prepared)

bikes.data.final <- bikes.data.prepared

bikes.data.final$datetime <- NULL
bikes.data.final$season <- NULL
bikes.data.final$atemp <- NULL
bikes.data.final$casual <- NULL
bikes.data.final$registered <- NULL
bikes.data.final$hour <- NULL

str(bikes.data.final)

#install.packages("caTools")
library(caTools)
filter <- sample.split(bikes.data.final$count, SplitRatio = 0.7)

#training set
bikes.train <- subset(bikes.data.final, filter == T)

#test set
bikes.test <- subset(bikes.data.final, filter == F)

dim(bikes.data.final)
dim(bikes.train)
dim(bikes.test)

model <- lm(count ~ . ,bikes.train )

summary(model)

predict.train <- predict(model, bikes.train)
predict.test <- predict(test)

#install.packages('lubridate')
library(lubridate)

#see prediction per day
predicted.all <- predict(model,bikes.data.final)

bikes.analysis <- bikes.data.final

bikes.analysis$predicted <- predicted.all 

datetime <- bikes.data.prepared$datetime

bikes.analysis$date <- as.Date(datetime)

bikes.analysis$month <- floor_date(bikes.analysis$date, "month")

bikes.month <- bikes.analysis %>% group_by(month.year) %>%
  summarize(actual=sum(count), predicted = sum(predicted))

ggplot() +
  geom_line(data = bikes.month, aes(month.year, actual), color ='red') +
  geom_line(data = bikes.month, aes(month.year, predicted), color ='blue')




