x <- 5
y <- 4

x
y
x[1] <- 6
x[2] <- 4
x[3] <- NULL

#expressions (vector operations)
y <- x*2
z <- x**2

#test data type
class(x)

#data type conversion
x <- as.character(x)
x <- as.integer(x)

#create vector with combine

x <- c(12,4,8)
x <- c(x,20,19)

#create vector with sequence
y <- 1:6
z <- 35:789

#subseting vectors
v <- c(45,56,87,94,56,56,77)
w <- v[3:6]
z <- v[5]
z <- v[c(1,6,7)]
z <- v[v>60] #filter
t <- v>60

# naming vector items
salary <- c(8000,6000,4000,4000) 
names(salary) <- c('jack','john','john','alice')
salary[1]
salary['jack']
names (salary)  

# a general method to create sequence
s <- seq(4,9,length=7)  

#random smapling
smp <- sample(v,10,replace = T)
smpl <- sample(v,5)   
?sample

#comuting mean
meanv <- mean(v)

v.with.na <- c(v,NA)
mean(v.with.na) #eror
mean(v.with.na, na.rm = T)

is.na(v.with.na)
filter.na <- is.na(v.with.na)
v.with.na[filter.na]
v.with.na[!filter.na]

#functions

tofar <- function(cel = 5){
  far <- cel*2 + 32
  return(far)
}

tofar(10)


