setwd("C:/Users/�����/OneDrive/������/loan")
loan.raw <- read.csv("loans.csv")

str(loan.raw)
summary(loan.raw)

loan.prepared <- loan.raw 

loan.prepared$Loan_ID <- NULL


table(loan.prepared$Loan_Status)

#Gender 
#change "" into 'Male' 

loan.prepared$Gender <- as.character(loan.prepared$Gender)

make_male <- function(x){
  if (x=="") return ('Male')
  return (x)
}

loan.prepared$Gender <- sapply(loan.prepared$Gender,make_male)
loan.prepared$Gender <- as.factor(loan.prepared$Gender)

summary(loan.prepared$Gender)

#Married 
#change "" into 'Yes' 


loan.prepared$Married <- as.character(loan.prepared$Married)

make_yes <- function(x){
  if (x=="") return ('Yes')
  return (x)
}

loan.prepared$Married <- sapply(loan.prepared$Married,make_yes)
loan.prepared$Married <- as.factor(loan.prepared$Married)

summary(loan.prepared$Married)


#Dependents

loan.prepared$Dependents <- as.character(loan.prepared$Dependents)

make_numbers <- function(x){
  if (x=='3+') return(3)
  if (x == "") return (0)
  return (x)
}

loan.prepared$Dependents <- sapply(loan.prepared$Dependents,make_numbers)
loan.prepared$Dependents <- as.integer(loan.prepared$Dependents)

summary(loan.prepared$Dependents)


#Self_Employed
summary(loan.prepared)
loan.prepared$Self_Employed <- as.character(loan.prepared$Self_Employed)

make_unknown <- function(x){
  if (x=="") return ('Unknown')
  return (x)
}

loan.prepared$Self_Employed <- sapply(loan.prepared$Self_Employed,make_unknown)
loan.prepared$Self_Employed <- as.factor(loan.prepared$Self_Employed)

summary(loan.prepared$Self_Employed)

#LoanAmount


make_average <- function(x,vec){
  if (is.na(x)) {
    return(mean(vec,na.rm = T))
  }
  return (x)
}


loan.prepared$Credit_History <- loan.raw$Credit_History

make_1 <- function(x){
  if (is.na(x)) {
    return(1)
  }
  return (x)
}

loan.prepared$Credit_History <- sapply(loan.prepared$Credit_History,make_1)

summary(loan.prepared)



#make_average(NA,loan.prepared$LoanAmount)

loan.prepared$LoanAmount <- sapply(loan.prepared$LoanAmount,make_average, vec = loan.prepared$LoanAmount)

summary(loan.prepared)


#Loan_Amount_Term
loan.prepared$Loan_Amount_Term <- sapply(loan.prepared$Loan_Amount_Term,make_average, vec = loan.prepared$Loan_Amount_Term)


#Credit_History
loan.prepared$Credit_History <- sapply(loan.prepared$Credit_History,make_average, vec = loan.prepared$Credit_History)

loan.prepared$Gender <- as.factor(loan.prepared$Gender)

summary(loan.prepared)
library(ggplot2)
str(loan.prepared)
#dev.off()

ggplot(loan.prepared, aes(Education)) + geom_bar()

#Education

ggplot(loan.prepared, aes(Education, fill = Loan_Status)) + geom_bar(position = 'fill')

#Credit_History
ggplot(loan.prepared, aes(Credit_History, fill = Loan_Status)) + geom_bar()

ggplot(loan.prepared, aes(Credit_History, fill = Loan_Status)) + geom_bar(position = 'fill')


#ApplicantIncome
ggplot(loan.prepared, aes(ApplicantIncome, fill = Loan_Status)) + geom_histogram(binwidth = 10000, position = 'fill')
ggplot(loan.prepared, aes(ApplicantIncome, fill = Loan_Status)) + geom_histogram(binwidth = 10000)


#Bulding the model 

library(caTools)
filter <- sample.split(loan.prepared$Loan_Status, SplitRatio = 0.7)

loan.train <- subset(loan.prepared, filter == TRUE)
loan.test <- subset(loan.prepared, filter == FALSE)

dim(loan.prepared)
dim(loan.train)
dim(loan.test)

loan.model <- glm(Loan_Status ~ ., family = binomial(link = "logit"), data = loan.train)

summary(loan.model)

pridected.loan.test <- predict(loan.model, newdata = loan.test, type = 'response')

#confusuion matrix 

confusion_matrix <- table(loan.test$Loan_Status,pridected.loan.test>0.6)

recall <- confusion_matrix[1,1] /(confusion_matrix[1,1] + confusion_matrix[1,2])
precision <- confusion_matrix[1,1] /(confusion_matrix[1,1] + confusion_matrix[2,1])

accuracy <- (confusion_matrix[1,1] + confusion_matrix[2,2])/length(loan.test$Loan_Status)

#Home work 

#1)

#The cost 
cost <- confusion_matrix[1,2]*70 + confusion_matrix[2,1]*30

#A genral function to compute cost 
compute.cost <- function(bl,cost1,cost2){
  confusion_matrix <- table(loan.test$Loan_Status, pridected.loan.test >bl)
  cost <- confusion_matrix[1,2]*cost1 + confusion_matrix[2,1]*cost2
  return (cost)
}

compute.cost(0.5,70,30)
compute.cost(0.9, 70, 30)


#Compute optimum boundary layer as a fucntion of cost 

#vector for computing the optimum 
bl.vec <- seq(0,1,length.out = 21)

#remove 0 and 0.95 and 1 
bl.vec <- bl.vec[2:19]

#Comnpute optimum 
results <- sapply(bl.vec,compute.cost, cost1 = 70, cost2 =30)
plot(bl.vec, results)

#A general function to compute optimum 
findmin <- function(FUN, vec, cost1, cost2){
  min = 10000000000
  for (item in vec){
    if (FUN(item, cost1, cost2)<min) {
      min <- FUN(item, cost1, cost2)
      bestitem <- item
    }  
  }
  return (c(bestitem,min))
}

findmin(compute.cost,bl.vec,70,30)

findmin(compute.cost,bl.vec,20,100)





