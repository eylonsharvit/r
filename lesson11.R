setwd("C:/Users/�����/OneDrive/������/spam")

spam.raw <- read.csv('spam.csv', stringsAsFactors = FALSE)

str(spam.raw)

spam <- spam.raw

spam$type <- as.factor(spam$type)
table(spam$type)

test <- as.integer(spam$type)

str(spam)

library(ggplot2)
ggplot(spam, aes(type)) + geom_bar()

head(spam)

#install.packages('tm')
library(tm)

spam.corpus <- Corpus(VectorSource(spam$text))
spam.corpus[[1]][[1]]
spam.corpus[[1]][[2]]
spam.corpus[[2]][[1]]

clean.corpus <- tm_map(spam.corpus, removePunctuation)
clean.corpus[[1]][[1]]

clean.corpus <- tm_map(clean.corpus, removeNumbers)

clean.corpus <- tm_map(clean.corpus, content_transformer(tolower))

clean.corpus <- tm_map(clean.corpus, removeWords, stopwords())

clean.corpus <- tm_map(clean.corpus, stripWhitespace)

dtm <- DocumentTermMatrix(clean.corpus)

dim(dtm)

dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,10))) 

dim(dtm.freq)

inspect(dtm.freq[1:10,1:20])

conv_01 <- function(x){
  x <- ifelse(x>0,1,0)
  return (as.integer(x))
}

dtm.final <- apply(dtm.freq, MARGIN = 1:2, conv_01)

dtm.df <- as.data.frame(dtm.final)

conv_01_type <- function(x){
  if (x =='ham') return(as.integer(0))
  return (as.integer(1))
}

spam$type <- sapply(spam$type, conv_01_type)

dtm.df$type <- spam$type

str(dtm.df)

#Split to test and train 

library(caTools)

filter <- sample.split(dtm.df$type,SplitRatio = 0.7 )

spam.train <- subset(dtm.df,filter==T)
spam.test <- subset(dtm.df,filter==F)


dim(spam.train)
dim(spam.test)

spam.model <- glm(type ~., family = binomial(link = 'logit'), data = spam.train)

summary(spam.model)

predition <- predict(spam.model, spam.test, type = 'response')

hist(predition)

actual <- spam.test$type

confusion_matrix <- table(actual,predition > 0.5)

precision <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[1,2])
recall <- confusion_matrix[2,2]/(confusion_matrix[2,2] + confusion_matrix[2,1]) 

#install.packages('ROSE')

library(ROSE)

spam.train.over <- ovun.sample(type~.,data = spam.train, method = 'over', N = 600)$data

table(spam.train.over$type)

spam.model.over <- glm(type ~., family = binomial(link = 'logit'), data = spam.train.over)

summary(spam.model.over)

predition.over <- predict(spam.model.over, spam.test, type = 'response')

hist(predition.over)

actual <- spam.test$type

confusion_matrix.over <- table(actual,predition.over > 0.5)

precision.over <- confusion_matrix.over[2,2]/(confusion_matrix.over[2,2] + confusion_matrix.over[1,2])
recall.over <- confusion_matrix.over[2,2]/(confusion_matrix.over[2,2] + confusion_matrix.over[2,1]) 



#Homework 

#Global variables: dtm, clean corpus, spam 

#Functions to use 
conv_01 <- function(x){
  x <- ifelse(x>0,1,0)
  return (as.integer(x))
}


conv_01_type <- function(x){
  if (x =='ham') return(as.integer(0))
  return (as.integer(1))
}


compute.cost <- function(bl,actual, predicted,cost1,cost2){
  confusion_matrix <- table(actual, predicted > bl)
  cost <- confusion_matrix[1,2]*cost1 + confusion_matrix[2,1]*cost2
  return (cost)
}


OptNofTrms <- function(n){
  dtm.freq <- DocumentTermMatrix(clean.corpus, list(dictionary = findFreqTerms(dtm,n)))
  dtm.final <- apply(dtm.freq, MARGIN = 1:2, conv_01)
  dtm.df <- as.data.frame(dtm.final)
  spam$type <- sapply(spam$type, conv_01_type)
  dtm.df$type <- spam$type
  filter <- sample.split(dtm.df$type,SplitRatio = 0.7 )
  spam.train <- subset(dtm.df,filter==T)
  spam.test <- subset(dtm.df,filter==F)
  spam.model <- glm(type ~., family = binomial(link = 'logit'), data = spam.train)
  prediction <- predict(spam.model, spam.test, type = 'response')
  actual <- spam.test$type
  
  #compute optimum   
  bl.vec <- seq(0,1,length.out = 21)
  bl.vec <- bl.vec[2:19]
  results <- sapply(bl.vec,compute.cost, actual = actual, predicted = prediction, cost1 = 1, cost2 =5)
  min = 10000000000
  i = 0
  for (item in results){
    i <- i +1
    if(item < min) {
      min <- item
      optyBL <- bl.vec[i]
    }  
  }
  return (c(min,optyBL))
}

#Examples runs
OptNofTrms(8)
OptNofTrms(9)
OptNofTrms(10)
OptNofTrms(11)
